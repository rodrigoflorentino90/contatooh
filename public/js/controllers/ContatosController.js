angular.module('contatooh').controller('ContatosController',
    function($scope, Contato){

        $scope.contatos = [];
        $scope.filtro =	'';
        $scope.mensagem	= {texto:''};

        $scope.remove = function(contato) {

            var	promise	= Contato.delete({id:contato._id}).$promise;

            promise
                .then(buscaContatos)
                .catch(function(erro){
                    $scope.mensagem = {texto:'Não foi possível remover o contato'};
                    console.log(erro);
                });

        };

        function buscaContatos() {
            Contato.query(
                function(contatos) {
                    $scope.contatos	= contatos;
                },
                function(erro) {
                    $scope.mensagem = {texto:'Não foi possível obter a lista'};
                    console.log(erro);
                }
            );
        }
        buscaContatos();

        /*
        $http.get('/contatos').success(function(data){
            $scope.contatos	= data;
        }).error(function(statusText){
            console.log("Não foi possível obter a lista de contatos");
            console.log(statusText);
        });


        $http.get('/contatos').then(successCallback, errorCallback);

        function successCallback(response){
            $scope.contatos	= response.data;
        }
        function errorCallback(error){
            console.log("Não foi possível obter a lista de contatos");
            console.log(statusText);
        }
        */
    }
);
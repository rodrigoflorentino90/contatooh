var ContatoPage = new require('./pages/contatoPage');

describe('Cadastro de contatos', function() {

    var pagina = new ContatoPage();

    beforeEach(function() {
        pagina.visitar();
    });

    it('Deve cadastrar um contato', function() {

        var aleatorio = Math.floor((Math.random() * 10000000) + 1);
        var	nome = 'teste' + aleatorio;
        var	email =	'teste@email' + aleatorio;

        pagina.digitarNome(nome);
        pagina.digitarEmail(email);

        element(by.cssContainingText('option', 'Rodrigo Florentino')).click();
        pagina.salvar();
        expect(element(by.binding('mensagem.texto')).getText()).toContain('sucesso');

    });

    it('Deve remover um contato da lista', function() {

        var	totalAntes = element.all(by.repeater('contato in contatos')).count();
        element(by.repeater('contato in contatos').row(0)).element(by.css('.btn')).click();

        var	totalDepois	= element.all(by.repeater('contato in contatos')).count();
        expect(totalDepois).toBeLessThan(totalAntes);

    });

});
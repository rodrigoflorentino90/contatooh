var	express	= require('express');
var load = require('express-load');
var	bodyParser = require('body-parser');
var	cookieParser = require('cookie-parser');
var	session	= require('express-session');
var	passport = require('passport');
var helmet = require('helmet');

require('dotenv-safe').load();

module.exports = function() {

    var	app	= express();

    //configuração de ambiente
    app.set('port',1000);

    //middleware
    app.use(express.static('./public'));
    app.set('view engine','ejs');
    app.set('views','./app/views');

    app.use(bodyParser.urlencoded({extended:true}));
    app.use(bodyParser.json());
    app.use(require('method-override')());

    app.use(cookieParser());
    app.use(
        session(
            {
                secret: process.env.SEGREDO,
                resave:	true,
                saveUninitialized: true
            }
        )
    );

    app.use(passport.initialize());

    app.use(helmet());

    //app.disable('x-powered-by');
    app.use(helmet.hidePoweredBy({setTo: 'PHP 5.5.14'})); //troca powerd-by for uma informação falsa
    app.use(helmet.frameguard()); //bloqueia o uso de i-frame
    app.use(helmet.xssFilter()); //bloqueia o uso de XSS
    app.use(helmet.noSniff()); // não permite o carregamento de MIME types inválidos

    app.use(passport.session());

    //home(app);
    load('models',{cwd:'app'})
        .then('controllers')
        .then('routes/auth.js')
        .then('routes')
        .into(app);

    app.get('*',function(req,res){
        res.status(404).render('404');
    });

	return	app;
};
var	mongoose = require('mongoose');

var options = {
  useMongoClient: true,
  autoIndex: false,
  reconnectTries: Number.MAX_VALUE,
  reconnectInterval: 500,
  poolSize: 30,
  bufferMaxEntries: 0
};

module.exports = function(uri,options) {

    mongoose.connect(uri);
    mongoose.set('debug',true);

    mongoose.connection.on('connected',	function() {
        console.log('Mongoose! Conectado em ' + uri);
    });

    mongoose.connection.on('disconnected', function() {
        console.log('Mongoose! Desconectado de ' + uri);
    });

    mongoose.connection.on('error', function(erro) {
        console.log('Mongoose! Erro na conexão: ' + erro);
    });

    process.on('SIGINT', function() {
        mongoose.connection.close(function() {
            console.log('Mongoose! Desconectado pelo término da aplicação');
    		process.exit(0);
        });
    });
}